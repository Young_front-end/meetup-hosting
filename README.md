## Introduction
Meet-up
Use the Google Maps api and meetup api to map meeting locations and provide meeting information.

## Client
1. Using modern Javascript
2. State management using redux
3. component-based UI

## Login
![ex_screenshot](./readme_img/로그인.gif)

## API
![ex_screenshot](./readme_img/meetup-API.gif)

## Add meeting
![ex_screenshot](./readme_img/즐겨찾기.gif)


## Map api
![ex_screenshot](./readme_img/지도api.gif)

[hosting link](https://youthful-lichterman-8e3fd6.netlify.com/)