import React, { Component } from "react";
import MapContainer from "./MapContainer";
import MeetupInfo from "./MeetupInfo";
import Home from "./Home";
import Loading from "./Loading";
import FavoriteList from "./FavoriteList";

import "./App.css";
import star from "../img/star_w.png";
import logo from "../img/logo.png";
import { startLat, startLng } from "../constants/api";
import { clickFavriteListBtn } from "../utill/utill";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showFavoriteList: false
    };
  }
  componentDidMount() {
    var accessUrl = window.location.href;
    var urlWords = accessUrl.split("/");
    var token = urlWords[3].split("&");
    var accessToken = token[0].split("=");
    if(this.props.onLoad) this.props.onLoad(accessToken[1], startLat, startLng);
  }

  render() {
    var isLogin = window.location.href.length;
    return (
      <div className="App">
        <div className="header">
          <div className="header-title">
            <img className="header-logo" src={logo} />
          </div>
          <div className="header-personal-info">
            <img className="header-personal-img" src={star} />
            <div
              onClick={clickFavriteListBtn.bind(this)}
              className="header-personal-text"
            >
              즐겨찾기
            </div>
          </div>
        </div>
        <div className="main">
          <div className="meet-map-wrapper">
            {this.props.data && (
              <MapContainer
                data={this.props.data.data}
                token={this.props.data.token}
                onload={this.props.onLoad}
                lat={this.props.data.lat}
                lng={this.props.data.lng}
              />
            )}
          </div>
          {isLogin > 60 ? (
            <MeetupInfo
              data={this.props.data}
              favoriteListData={this.props.favoriteListData}
              favoriteUpdate={this.props.favoriteUpdate.bind(this)}
            />
          ) : (
            <Home />
          )}
          {this.props.isLoading && <Loading />}
        </div>
        <div
          style={{
            display:
              this.state.showFavoriteList &&
              this.props.favoriteListData.length !== 0
                ? "block"
                : "none"
          }}
          className="fav-modal"
        >
          {this.props.favoriteListData &&
            this.props.favoriteListData.map((event, i) => {
              return (
                <FavoriteList
                  name={event.name}
                  date={event.date}
                  groupName={event.groupName}
                  rsvp={event.rsvp}
                  id={event.id}
                  favoriteListData={this.props.favoriteListData}
                  favoriteUpdate={this.props.favoriteUpdate.bind(this)}
                  key={i}
                />
              );
            })}
        </div>
      </div>
    );
  }
}

export default App;
