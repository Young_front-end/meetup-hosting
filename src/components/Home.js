import React, { Component } from "react";
import { url } from "../constants/api";
import "./Home.css";

export class Home extends Component {
  render() {
    return (
      <div className="home-info-wrapper">
        <div className="home-info-text">
          Oauth 는 외부서비스의 인증 및 권한부여를 관리하는 범용적인
          프로토콜입니다.
          <br />* 권한 : OAuth는 인증뿐만 아니라 권한도 관리합니다. 사용자의
          권한에 따라 접근할 수 있는 데이터가 다르도록 설정이 가능합니다.
          <br /> * 프로토콜 : 특정한 프로그램을 지칭하는게 아니라 일종의
          규격입니다. Facebook, Google, Naver 등은 OAuth라는 규격에 맞춰 인증 및
          권한을 대행관리 해줍니다.
          <br />* 외부서비스 : 우리가 만들고 있는 서비스를 이야기합니다. 외부
          서비스를 위한 서비스인 OAuth는 우리 서비스의 인증 및 권한부여를 관리를
          대행해줍니다.
        </div>
        <a className="home-info-link" href={url}>
          <div className="home-info">시작하기</div>
        </a>
      </div>
    );
  }
}

export default Home;
