import React, { Component } from "react";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import { onMapClicked, onMarkerClick } from "../utill/utill";

import "./MapContainer.css";

import { mapApiKey } from "../constants/api";
import { centerMoved } from "../utill/utill";

export class MapContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hostName: null,
      photourl: null,
      groupName: null,
      eventName: null,
      date: null,
      count: null,
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {}
    };
  }

  render() {
    const style = {
      width: "50%",
      height: "100%"
    };
    var points = [];
    if (this.props.data) {
      for (var i = 0; i < this.props.data.events.length; i++) {
        points.push({
          lat: this.props.data.events[i].group.lat,
          lng: this.props.data.events[i].group.lon
        });
      }
    }
    return (
      <div className="meet-map2-wrapper">
        <Map
          google={this.props.google}
          style={style}
          center={{
            lat: this.props.lat,
            lng: this.props.lng
          }}
          zoom={10}
          onClick={onMapClicked}
          onDragend={centerMoved.bind(this)}
        >
          {points.map((value, i) => {
            return (
              <Marker
                onClick={onMarkerClick.bind(
                  this,
                  this.props.token,
                  this.props.data.events[i].id,
                  this.props.data.events[i].group.urlname,
                  this.props.data.events[i].group.name,
                  this.props.data.events[i].name,
                  this.props.data.events[i].local_date,
                  this.props.data.events[i].yes_rsvp_count
                )}
                position={value}
                key={i}
              />
            );
          })}
          <InfoWindow
            marker={this.state.activeMarker}
            visible={this.state.showingInfoWindow}
          >
            <div className="info-box">
              <div className="events-info">
                <p className="events-info-text-host">
                  HOST : {this.state.hostName}
                </p>
                <p className="events-info-text">
                  GROUP : {this.state.groupName}
                </p>
                <p className="events-info-text">
                  EVENTS : {this.state.eventName}
                </p>
                <p className="events-info-text">DATE : {this.state.date}</p>
                <p className="events-info-text">
                  PARTICIPANTS : {this.state.count}
                </p>
              </div>
              <div className="events-img-wrapper">
                <img className="host-img" src={this.state.photourl} />
              </div>
            </div>
          </InfoWindow>
        </Map>
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: mapApiKey
})(MapContainer);
