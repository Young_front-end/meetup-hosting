import React, { Component } from "react";
import { clickFavrite } from "../utill/utill";
import "./MeetupInfoList.css";
import plus from "../img/plus.png";
import Delete from "../img/delete.png";

export class MeetupInfoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isClicked: false,
      clickedImg: plus,
      favoriteList: []
    };
  }
  render() {
    return (
      <div className="events-wrapper">
        <div className="events-name">EVENTS NAME : {this.props.name}</div>
        <div className="events-date">DATE : {this.props.date}</div>
        <div className="events-group-name">MEETUP GROUP NAME : {this.props.groupName}</div>
        <div className="events-participants">THE NUMBER OF PARTICIPANTS : {this.props.rsvp}</div>
        <div className="favorite">
          <img
            onClick={clickFavrite.bind(
              this,
              this.props.name,
              this.props.date,
              this.props.groupName,
              this.props.rsvp,
              this.props.id,
              this.props.favoriteListData
            )}
            className="favorite-img"
            src={this.state.clickedImg}
          />
        </div>
      </div>
    );
  }
}

export default MeetupInfoList;
