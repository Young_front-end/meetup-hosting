import React, { Component } from "react";
import "./FavoriteList.css";
import Delete from "../img/delete.png";

export class FavoriteList extends Component {
  render() {
    return (
      <div className="favorite-events-wrapper">
        <div className="favorite-events-name">NAME : {this.props.name}</div>
        <div className="favorite-events-text">DATE : {this.props.date}</div>
        <div className="favorite-events-text">MEETUP GROUP NAME : {this.props.groupName}</div>
        <div className="favorite-events-text">THE NUMBER OF PARTICIPANTS : {this.props.rsvp}</div>
        <div className="favorite-events-btn">
          <img
            onClick={this.props.favoriteUpdate.bind(
              this,
              this.props.name,
              this.props.date,
              this.props.groupName,
              this.props.rsvp,
              this.props.id,
              this.props.favoriteListData
            )}
            className="favorite-delete"
            src={Delete}
          />
        </div>
      </div>
    );
  }
}

export default FavoriteList;
