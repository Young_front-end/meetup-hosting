import React from "react";
import { shallow, configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import FavoriteList from "./FavoriteList";

configure({ adapter: new Adapter() });

describe("FavoriteList compoment test", () => {
  let component;
  let mockUpdate;

  let mockName = "Sunday Board Gaming at N&N Adventuring Company";
  let mockDate = "2019-09-01";
  let mockGroupName = "The Game Zone!";
  let mockrsvp = 5;
  let mockId = 31197770;
  let mockListData = [1, 2];

  beforeEach(() => {
    mockUpdate = jest.fn();

    component = shallow(
      <FavoriteList
        name={mockName}
        date={mockDate}
        groupName={mockGroupName}
        rsvp={mockrsvp}
        id={mockId}
        favoriteListData={mockListData}
        favoriteUpdate={mockUpdate}
        key={1}
      />
    );
  });
  it("1. matches snapshot", () => {
    expect(component.debug()).toMatchSnapshot();
  });

  it("2. component text test", () => {
    const friendDiv = component.find(".favorite-events-name");
    expect(friendDiv.text()).toBe(`NAME : ${mockName}`);
  });

  it("3. calls functions test", () => {
    const mockUpdate = jest.fn();
    component = mount(
      <FavoriteList
        name={mockName}
        date={mockDate}
        groupName={mockGroupName}
        rsvp={mockrsvp}
        id={mockId}
        favoriteListData={mockListData}
        favoriteUpdate={mockUpdate}
        key={1}
      />
    );
    const img = component.find("img");
    img.simulate("click");
    expect(mockUpdate.mock.calls.length).toBe(1);
  });
});
