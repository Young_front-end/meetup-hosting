import React, { Component } from "react";
import "./Loading.css";

import loading from "../img/loading.gif";

class Loading extends Component {
  render() {
    return (
      <div className='loading-wrapper'>
        <img className="loading-img" src={loading} />
      </div>
    );
  }
}

export default Loading;
