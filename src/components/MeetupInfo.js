import React, { Component } from "react";
import MeetupInfoList from "./MeetupInfoList";
import "./MeetupInfo.css";

export class MeetupInfo extends Component {
  render() {
    return (
      <div className="meet-info-wrapper">
        {this.props.data.data &&
          this.props.data.data.events.map((event, i) => {
            return (
              <MeetupInfoList
                name={event.name}
                date={event.local_date}
                groupName={event.group.name}
                rsvp={event.yes_rsvp_count}
                id={event.id}
                favoriteListData={this.props.favoriteListData}
                favoriteUpdate={this.props.favoriteUpdate}
                key={i}
              />
            );
          })}
      </div>
    );
  }
}

export default MeetupInfo;
