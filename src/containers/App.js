import { connect } from "react-redux";
import { getMeetupInfo } from "../api/index";
import App from "../components/App";
const mapStateToProps = state => {
  return {
    data: state.data,
    isLoading: state.isLoading,
    favoriteListData: state.favoriteListData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLoad(token, startLat, startLng) {
      dispatch({
        type: "LOADING_ON"
      });
      getMeetupInfo(token, startLat, startLng).then(data => {
        dispatch({
          type: "MEETUP_DATA_LOAD",
          data
        });
      });
    },
    favoriteUpdate(name, date, groupName, rsvp, id, favoriteList) {
      if (favoriteList.length === 0) {
        localStorage.setItem(
          "userInfo",
          JSON.stringify({
            name: name,
            date: date,
            groupName: groupName,
            rsvp: rsvp,
            id: id
          })
        );
        var favoriteListData = [];
        favoriteListData.push(JSON.parse(localStorage.userInfo));
      } else {
        var favoriteListData = [];
        for (var i = 0; i < favoriteList.length; i++) {
          if (favoriteList[i].id !== id) favoriteListData.push(favoriteList[i]);
        }
        if (favoriteList.length === favoriteListData.length) {
          favoriteListData.push({
            name: name,
            date: date,
            groupName: groupName,
            rsvp: rsvp,
            id: id
          });
        }
        localStorage.setItem("userInfo", JSON.stringify(favoriteListData));
      }
      dispatch({ type: "FAVORITE_UPDATE", favoriteListData });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
