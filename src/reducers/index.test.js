import {
  meetupInfoReducer,
  loadingReducer,
  favoriteReducer
} from "../reducers";

describe("reducer test : should return the initial state", () => {
  const initialState = { data: [], isLoading: false, favoriteListData: [] };

  it("1. meetupInfoReducer test", () => {
    expect(meetupInfoReducer(initialState, {})).toEqual({
      data: [],
      isLoading: false,
      favoriteListData: []
    });
  });
  it("2. loadingReducer test", () => {
    expect(loadingReducer(initialState, {})).toEqual({
      data: [],
      isLoading: false,
      favoriteListData: []
    });
  });
  it("3. favoriteReducer test", () => {
    expect(favoriteReducer(initialState, {})).toEqual({
      data: [],
      isLoading: false,
      favoriteListData: []
    });
  });
});

describe("meetupInfoReducer detail test", () => {
  const initialState = { data: [] };
  const action = {
    type: "MEETUP_DATA_LOAD",
    data: { id: 1 }
  };
  it("4. meetupInfoReducer case MEETUP_DATA_LOAD", () => {
    expect(meetupInfoReducer(initialState.data, action)).toEqual({ id: 1 });
  });
  const initialState1 = { data: [] };
  const action1 = {
    type: "MEETUP_DATA_LOAD",
    data: {}
  };
  it("5. meetupInfoReducer case MEETUP_DATA_LOAD 2", () => {
    expect(meetupInfoReducer(initialState1.data, action1)).toEqual({});
  });

  describe("loadingReducer detail test", () => {
    const initialState1 = { isLoading: false };
    const action1 = { type: "MEETUP_DATA_LOAD" };
    it("6. loadingReducer case MEETUP_DATA_LOAD", () => {
      expect(loadingReducer(initialState1.isLoading, action1)).toEqual(false);
    });
    const initialState2 = { isLoading: false };
    const action2 = { type: "FAVORITE_UPDATE" };
    it("7. loadingReducer case FAVORITE_UPDATE", () => {
      expect(loadingReducer(initialState2.isLoading, action2)).toEqual(false);
    });
    const initialState3 = { isLoading: false };
    const action3 = { type: "LOADING_ON" };
    it("8. loadingReducer case LOADING_ON", () => {
      expect(loadingReducer(initialState3.isLoading, action3)).toEqual(true);
    });
  });
});

describe("reducer immutable test ", () => {
  const initialState = { data: [], isLoading: false, favoriteListData: [] };
  Object.freeze(initialState);
  it("9. meetupInfoReducer immutable test", () => {
    expect(meetupInfoReducer(initialState, {})).toEqual({
      data: [],
      isLoading: false,
      favoriteListData: []
    });
  });
  it("10. loadingReducer immutable test", () => {
    expect(loadingReducer(initialState, {})).toEqual({
      data: [],
      isLoading: false,
      favoriteListData: []
    });
  });
  it("11. favoriteReducer immutable test", () => {
    expect(favoriteReducer(initialState, {})).toEqual({
      data: [],
      isLoading: false,
      favoriteListData: []
    });
  });
});
