import { combineReducers } from "redux";

const initialState = {
  data: [],
  isLoading: false,
  favoriteListData: []
};

export function meetupInfoReducer(state = initialState.data, action) {
  switch (action.type) {
    case "MEETUP_DATA_LOAD":
      var newState1;
      if (action.data) newState1 = action.data;
      return newState1;
    default:
      return state;
  }
}
export function loadingReducer(state = initialState.isLoading, action) {
  switch (action.type) {
    case "MEETUP_DATA_LOAD":
      return false;
    case "LOADING_ON":
      return true;
    default:
      return state;
  }
}

export function favoriteReducer(state = initialState.favoriteListData, action) {
  switch (action.type) {
    case "FAVORITE_UPDATE":
      var newState;
      if (action.favoriteListData) newState = action.favoriteListData;
      return newState;
    default:
      return state;
  }
}

export default combineReducers({
  data: meetupInfoReducer,
  isLoading: loadingReducer,
  favoriteListData: favoriteReducer
});
