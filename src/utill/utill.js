import axios from "axios";
import plus from "../img/plus.png";
import Delete from "../img/delete.png";

export function onMapClicked(props) {
  if (this.state.showingInfoWindow) {
    this.setState({
      showingInfoWindow: false,
      activeMarker: null
    });
  }
}

export function onMarkerClick(
  token,
  eventId,
  urlName,
  groupName,
  eventName,
  date,
  count,
  props,
  marker,
  e
) {
  const url = `https://cors-anywhere.herokuapp.com/api.meetup.com/${urlName}/events/${eventId}/hosts?&sign=true&photo-host=public`;
  axios
    .get(url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => {
      this.setState({
        hostName: res.data[0].name,
        photourl: res.data[0].photo.photo_link,
        groupName: groupName,
        eventName: eventName,
        date: date,
        count: count,
        selectedPlace: props,
        activeMarker: marker,
        showingInfoWindow: true
      });
    })
    .catch(function(error) {
      return error;
    });
}

export function clickFavrite(name, date, groupName, rsvp, id, favoriteList) {
  this.props.favoriteUpdate(name, date, groupName, rsvp, id, favoriteList);
  if (!this.state.isClicked) {
    this.setState({
      isClicked: true,
      clickedImg: Delete
    });
  } else {
    this.setState({
      isClicked: false,
      clickedImg: plus
    });
  }
}

export function centerMoved(mapProps, map) {
  const lat = map.center.lat();
  const lng = map.center.lng();
  this.props.onload(this.props.token, lat, lng);
}
export function clickFavriteListBtn() {
  this.setState({
    showFavoriteList: !this.state.showFavoriteList
  });
}
