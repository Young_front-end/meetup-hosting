import axios from "axios";

export const getMeetupInfo = (token, Lat, Lng) => {
  return axios
    .get(
      `https://cors-anywhere.herokuapp.com/api.meetup.com/find/upcoming_events?&sign=true&photo-host=secure&lon=${Lng}&page=10&lat=${Lat}`,
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    )
    .then(res => {
      res.token = token;
      res.lat = Lat;
      res.lng = Lng; 
      return res;
    })
    .catch(function(error) {
      return error;
    });
};

export const getHostInfo = (token, eventId, urlName) => {
  const url = `https://cors-anywhere.herokuapp.com/api.meetup.com/${urlName}/events/${eventId}/hosts?&sign=true&photo-host=public`;
  axios
    .get(url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => {
      return res;
    })
    .catch(function(error) {
      return error;
    });
};
